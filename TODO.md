# TO-DO list
##Information
This document is used as a list of TO-DOs and other plans.

 [X] _IN PROGRESS_ Implement the login panel.
  ^       ^                 ^
  |       |                 |
  |       |        Description of the plan.
  |       |
  |      Must be _On Hold_, _In Development_, or _Finished_.
  |
  ``X`` if the feature is done.

## General
+ [ ] _On Hold_ More logging verbosity.
+ [ ] _On Hold_ Module logging.

## Web Interface
+ [ ] _On Hold_ Need to fix login system. Use login module.
+ [ ] _On Hold_ API support continuation. (Escape characters, return outputs, etc.)

## Framework Commands
+ [ ] _On Hold_ Use subprocess on ``run`` command because os.system() is deprecated.
+ [ ] _On Hold_ Built-In Notepad.
+ [ ] _On Hold_ Module Manager (Included in `module` command.)

## Framework Modules
+ [ ] 1.Reconnaissance Tools
    - [ ] _In Development_ ReconMe (Web Reconnaissance Tool)
        * [X] More stable CMS detection.
    - [ ] _On Hold_ WhatsYourName (Get host by a number of ways)
    - [ ] _On Hold_ IPCalc (IP Calculator)

+ [ ] 2.Scanning Tools
    - [ ] _On Hold_ ArcháriosScanner
    - [ ] _On Hold_ Sensitive File Detector
    - [ ] _On Hold_ Sub-Domain Scanner
    - [ ] _On Hold_ NetScan (Like Nmap)
    - [ ] _On Hold_ SimpleWP (WordPress Scanner and Backup Grabber)

+ [ ] 3.Vulnerability Analysis Tools
    - [ ] _On Hold_ ArcháriosFlooder (DoS Tool)
    - [ ] _On Hold_ WPEnum (WordPress Username Enumerator)
    - [ ] _On Hold_ Cross-Site Scripter (XSS Vulnerability Analysis Tool)

+ [ ] 4.Database Assessment Tools
    - [ ] _On Hold_ SQLiScan (SQL Injection Vulnerability Scanner)

+ [ ] 5.Password Attacks
    - [ ] _On Hold_ Hypothesis (Wordlist generator)
    - [ ] _On Hold_ CipherCracker (Encryption, Decryption, and Cracking Tool; Cipher Identifier)
    - [ ] _On Hold_ DefaultPass (Gather default credentials using various sources.)
    - [ ] _On Hold_ RARCrack (RAR Archive password cracker)

+ [ ] 6.Wireless Auditing Tools
    - [ ] _On Hold_ Kick 'Em (Kick devices out of the network.)
    - [ ] _On Hold_ WiCrack (WiFi Cracking Tool)
    - [ ] _On Hold_ Bluetooth Predator (Bluetooth Attacks)

+ [ ] 8.Exploitation Tools
    - [ ] ShadowSploit
    - [ ] SuggestMeExploit (Exploit Suggesting Tool using various sources.)

+ [ ] 9.Post-Exploitation Tools
    - [ ] BRAT (Basic Remote Administration Tool)

+ [ ] 12.Reporting Tools
    - [ ] Panalyze (Password Analyzer)
    - [ ] Dtctv (Server/Client File Repository for Investigating cyber attacks.)

+ [ ] 13.Social Engineering Tools
    - [ ] DarkFish (HTTP server for phishing)

+ [ ] 14.System Services
    - [ ] Deception (HTTP, FTP, SMTP, Telnet, SSH, and SNMP honeypot)
    - [ ] WebExpose
    - [ ] SimpleIM_Server (Chat server that uses raw sockets and actual IP for connection)

+ [ ] 15.Others
    - [X] Ipify (Know your external IP Address)
    - [X] MapSCII (ASCII Map via Telnet)
    - [ ] SimpleIM (SimpleIM client)
